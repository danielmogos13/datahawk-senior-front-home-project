import { AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DatasetId } from '../../../../models/dataset-id.enum';
import * as $ from 'jquery';
import * as daterangepicker from 'daterangepicker';
import { Moment } from "moment";
import { Select, Store } from "@ngxs/store";
import { AppState } from "../../../../state/app.state";
import { Observable } from "rxjs";
import { DateRange } from "../../../../models/date-range";
import { AppActions } from "../../../../state/app.actions";
import { DisplayedRanks } from "../../../../models/displayed-ranks";

@Component({
  selector: 'dh-rank-viewer-filters',
  templateUrl: './rank-viewer-filters.component.html',
  styleUrls: ['./rank-viewer-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RankViewerFiltersComponent implements OnInit, AfterViewInit {
  @Input() selectedId: DatasetId | null = null;
  @Input() datasetIds: DatasetId[] | null = [];

  @Output() datasetSelect: EventEmitter<DatasetId> = new EventEmitter<DatasetId>();

  // @ts-ignore
  @Select(AppState.selectedDateRange) selectedDateRange: Observable<DateRange>;

  // @ts-ignore
  @Select(AppState.selectedDisplayedRanks) displayedRanks: Observable<DisplayedRanks>;

  daterangepicker: any = daterangepicker;
  minRank: number = 1;
  maxRank: number = 20;

  constructor(private readonly store: Store) { }

  ngOnInit(): void {
    this.displayedRanks.subscribe(ranks => {
      this.minRank = ranks.minRank;
      this.maxRank = ranks.maxRank;
    });
  }

  ngAfterViewInit() {
    this.selectedDateRange.subscribe(dateRange => {
      $('.date-range-select').daterangepicker({
        maxSpan: {
          "days": 7
        },
        startDate: dateRange.startDate,
        endDate: dateRange.endDate,
      }, this.onDateRangeSelected.bind(this) );
    });
  }

  onDatasetClick(datasetId: string) {
    this.datasetSelect.emit(datasetId as DatasetId);
  }

  onDateRangeSelected(startDate: Moment, endDate: Moment): void {
    this.store.dispatch(new AppActions.SelectDateRange({startDate, endDate}));
  }

  onMinRankChanged() {
    if (!this.isInteger(this.minRank) && !this.isValidRank()) {
      return;
    }

    this.store.dispatch(new AppActions.SelectDisplayedRanks(this.getRanks()));
  }

  onMaxRankChanged () {
    if (!this.isInteger(this.maxRank) && !this.isValidRank()) {
      return;
    }

    this.store.dispatch(new AppActions.SelectDisplayedRanks(this.getRanks()));
  }

  getRanks = () => {
    return {
      minRank: Number(this.minRank),
      maxRank: Number(this.maxRank)
    }
  }

  isInteger = (number: number) => Number.isInteger(Number(number));

  isValidRank = () => {
    const maxRankIsBiggerThankMinRank = this.maxRank > this.minRank;
    const maxRankIsLessThanMaximumDisplayed = this.maxRank <= 100;
    const minRankIsBiggerThanZero = this.minRank > 0;

    return maxRankIsBiggerThankMinRank && maxRankIsLessThanMaximumDisplayed && minRankIsBiggerThanZero;
  }
}
