import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { AppActions } from './app.actions';
import { DatasetId } from '../models/dataset-id.enum';
import { ProductRank } from '../models/product-rank.type';
import { BedroomFurnitureBSROverTime } from '../../assets/dataset/BSR/bedroom-furniture.dataset';
import { MattressesAndBoxSpringsBSROverTime } from '../../assets/dataset/BSR/mattresses-and-box-springs.dataset';
import { FurnitureBSROverTime } from '../../assets/dataset/BSR/furniture.dataset';
import * as moment from 'moment';
import { DateRange } from "../models/date-range";
import { DisplayedRanks } from "../models/displayed-ranks";

export interface AppStateModel {
  dataset: { [key in DatasetId]: ProductRank[] };
  selectedDatasetId: DatasetId;
  dateRange: DateRange;
  displayedRanks: DisplayedRanks;
}

const defaults: AppStateModel = {
  displayedRanks: {
    minRank: 1,
    maxRank: 100
  },
  dateRange: {
    startDate: moment('11/07/2019', 'MM/DD/YYYY').utc(true),
    endDate: moment('11/13/2019', 'MM/DD/YYYY').utc(true)
  },
  dataset: {
    [DatasetId.BSR_FURNITURE]: FurnitureBSROverTime,
    [DatasetId.BSR_BEDROOM_FURNITURE]: BedroomFurnitureBSROverTime,
    [DatasetId.BSR_MATTRESSES_AND_BOX_SPRINGS]: MattressesAndBoxSpringsBSROverTime,
  },
  selectedDatasetId: DatasetId.BSR_FURNITURE
}

@State<AppStateModel>({
  name: 'app',
  defaults
})
@Injectable()
export class AppState {
  constructor() {
  }

  @Selector()
  public static selectedDataset(state: AppStateModel): ProductRank[] {
    const dataset = state.dataset[state.selectedDatasetId];

    const products = this.filterProducts(dataset, state.dateRange);

    return this.limitProducts(products, state.displayedRanks);
  }

  @Selector()
  public static selectedDatasetId(state: AppStateModel): DatasetId {
    return state.selectedDatasetId;
  }

  @Selector()
  public static selectedDateRange(state: AppStateModel): DateRange {
    return state.dateRange;
  }

  @Selector()
  public static selectedDisplayedRanks(state: AppStateModel): DisplayedRanks {
    return state.displayedRanks;
  }

  @Action(AppActions.SelectDataset)
  selectDataset({ patchState }: StateContext<AppStateModel>, { datasetId }: AppActions.SelectDataset) {
    patchState({ selectedDatasetId: datasetId });
  }

  @Action(AppActions.SelectDateRange)
  selectDateRange({ patchState }: StateContext<AppStateModel>, { dateRange }: AppActions.SelectDateRange) {
    patchState({ dateRange: dateRange });
  }

  @Action(AppActions.SelectDisplayedRanks)
  selectDisplayedRanks({ patchState }: StateContext<AppStateModel>, { ranks }: AppActions.SelectDisplayedRanks) {
    patchState({ displayedRanks: ranks });
  }

  public static filterProducts (dataset: ProductRank[], dateRange: DateRange) {
    return dataset.filter((p) =>
        moment(p.date, 'MM/DD/YYYY').utc(true)
            .isBetween(dateRange.startDate, dateRange.endDate, null, '[]'))
  }

  public static limitProducts (dataset: ProductRank[], displayedRanks: DisplayedRanks) {
    return dataset.filter((product) => product.rank >= displayedRanks.minRank && product.rank <= displayedRanks.maxRank)
  }
}
