import { DatasetId } from '../models/dataset-id.enum';
import { DateRange } from "../models/date-range";
import { DisplayedRanks } from "../models/displayed-ranks";

export namespace AppActions {
  export class SelectDataset {
    static readonly type = '[App] select dataset';
    constructor(public datasetId: DatasetId) { }
  }

  export class SelectDateRange {
    static readonly type = '[App] select daterange';
    constructor(public dateRange: DateRange) { }
  }

  export class SelectDisplayedRanks {
    static readonly type = '[App] select ranks';
    constructor(public ranks: DisplayedRanks) { }
  }
}
