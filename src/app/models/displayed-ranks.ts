export interface DisplayedRanks {
    minRank: number,
    maxRank: number
}
